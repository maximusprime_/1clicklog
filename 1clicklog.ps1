# Script pour se connecter en un clic au pare-feu de la rez

# ATTENTION
# Le login est le mot de passe seront EN CLAIR dans ce script
# En autorisant ce sript à être executé, votre PC pourra aussi exécuter N'IMPORTE QUEL SCRIPT Powershell
# Utilisez ce script en connaissance de cause

$utilisateur = "utilisateur"
$motdepasse = "motdepasse"
$url = "https://fw-cgcp.emse.fr/auth/plain.html?uid=$utilisateur&pswd=$motdepasse&time=480"
$ssid = (get-netconnectionProfile).Name

$ie = New-Object -ComObject 'internetExplorer.Application'
$ie.visible = $false
$ie.navigate($url)
start-sleep 1
$session = $ie.Document.getElementsByName("session")[0].value
$ie.navigate("https://fw-cgcp.emse.fr/auth/disclaimer.html?session=$session&read=accepted&action=J'accepte")
start-sleep 1
$ie.Quit()