# 1clicklog

## Introduction

Ce programme permet de se connecter en un clic au réseau de la résidence, en WiFi ou ethernet, à l'aide d'un script powershell.

## AVERTISSEMENTS

1. Ce script contient votre mot de passe EN CLAIR
2. Pour exécuter ce script, il faut modifier la politique d'exécution de powershell pour l'autoriser à exécuter n'importe quel script
3. Ce programme n'est comptaible qu'avec Windows (Testé sur W10, probablement compatible avec W7, W8 et W11)

## Installation

### Téléchargement

Vous aurez besoin de télécharger les fichiers `1clicklog.ps1` et `1clicklog.ink`

### Mise en place

Ouvrez avec le bloc-note (ou votre éditeur de code préféré) le fichier `1clicklog.ps1`
Remplacez `utilisateur` par votre identifiant EMSE et `motdepasse` par votre mot de passe EMSE

![](readme/img.jpg)

Enregistrez et quittez

Copiez ce fichier édité et collez-le à la racine de votre disque dur, directement dans `C:\`. Vous pouvez y accéder en allant sur « Ce PC » dans l’explorateur de fichiers. L’explorateur vous demandera l’autorisation administrateur pour le coller ici. Acceptez et continuez.
Si vous ne l'avez pas déjà copiez et collez le raccourci `1clicklog(.ink)` sur votre bureau.

Il ne vous reste plus qu'à cliquer sur ce raccourci pour vous connecter.